# Python 3 - Alura basico (partes 1 e 2)

## Projeto de jogos (Advinhação, Forca e um inicializador para usuário escolher qual jogo jogar).

### Conceitos abordados:
- Criação de variáveis;
- Tipos de dados;
- Strings;
- Estruturas de decisão (if, elif e else);
- Estruturas de repetição (for e while);
- Coleções (tuplas, listas, dicionários e set);
- List comprehension (ex: i = [x for x in y]);
- Funções;
- Etc...

![forcaespanholito_a](/uploads/108d6061c445c2d5c568d108186589e9/forcaespanholito_a.gif)