import jogo_advinhacao
import jogo_forca


def escolher_jogo():
    print('*' * 29)
    print('**** Escolha o seu jogo! ****')
    print('*' * 29)

    print('(1) Forca (2) Advinhação')

    jogo = int(input('Qual jogo? '))

    if jogo == 1:
        print('Jogando Forca')
        jogo_forca.jogar()
    elif jogo == 2:
        print('Jogando Advinhacao')
        jogo_advinhacao.jogar()


if __name__ == '__main__':
    escolher_jogo()
