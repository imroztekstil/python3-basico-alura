import random


def jogar():
    print('*' * 38)
    print('** Bem vindo ao jogo de Advinhação! **')
    print('*' * 38)

    numero_secreto = random.randint(0, 100)

    print('Qual nível de dificuldade?')
    print('(1) Fácil (2) Médio (3) Difícil')

    nivel = int(input('Define o nível: '))
    if nivel == 1:
        total_de_tentativas = 20
    elif nivel == 2:
        total_de_tentativas = 10
    else:
        total_de_tentativas = 5

    for tentativa in range(1, total_de_tentativas + 1):
        print(f'Tentativa nº{tentativa}')
        chute = int(input('Digite o seu Numero entre 0 e 100: '))

        if chute < 1 or chute > 100:
            print('Entrada inválida! Perdeu uma tentativa!')
            continue

        if chute == numero_secreto:
            print(f'Você acertou na {tentativa} tentativa!')
            break
        elif chute > numero_secreto:
            print(f'Você errou! Seu chute foi maior que o número!')
        else:
            print(f'Você errou! Seu chute foi menor que no número!')

        print('="=' * 15)

    print('Fim de jogo...')


if __name__ == '__main__':
    jogar()
